<?php

$name = $argv[1] ?? null;
if ($name === null) {
    echo 'Name is not set';
    echo "\n";
} else {
    echo "Hello, {$name}!\n";
}

